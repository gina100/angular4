﻿using System;

namespace AngularLogin4.Models
{
    public partial class Schedule
    {
        public int ScheduleId { get; set; }
        public DateTime ScheduleStartDate { get; set; }
        public DateTime ScheduleEndDate { get; set; }
        public string ThemeColor { get; set; }
        public bool Archived { get; set; }
        public string Location { get; set; }
        public int? SeniorStudentId { get; set; }
        public int? StudentId { get; set; }
    }
}
