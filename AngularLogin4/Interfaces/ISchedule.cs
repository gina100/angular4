﻿using AngularLogin4.Models;
using System.Collections.Generic;

namespace AngularLogin4.Interfaces
{
    public interface ISchedule
    {
        IEnumerable<Schedule> GetAllSchedules();
        int AddSchedule(Schedule schedule);
        int UpdateSchedule(Schedule schedule);
        Schedule GetScheduleData(int id);
        int DeleteSchedule(int id);
    }
}
