﻿
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AngularLogin4.Models;
using AngularLogin4.Interfaces;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AngularLogin4.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly ISchedule objSchedule;

        public ScheduleController(ISchedule _objSchedule) 
        {
            objSchedule = _objSchedule;
        }

        [HttpGet]
        [Route("Index")]
        public IEnumerable<Schedule> Index()
        {
            return objSchedule.GetAllSchedules();
        }

        [HttpGet]
        [Route("GetworkSchedules")]
        public IEnumerable<Schedule> GetworkSchedules()
        {
            return  objSchedule.GetAllSchedules();
        }

        [HttpPost]
        [Route("Create")]
        public int Create([FromBody] Schedule schedule)
        {
            return objSchedule.AddSchedule(schedule);
        }

        [HttpPut]
        [Route("Edit")]
        public int Edit([FromBody] Schedule schedule)
        {
            return objSchedule.UpdateSchedule(schedule);
        }
        [HttpGet]
        [Route("Details/{id}")]
        public Schedule Details(int id)
        {
            return objSchedule.GetScheduleData(id);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public int Delete(int id)
        {
            return objSchedule.DeleteSchedule(id);
        }

    }
}
