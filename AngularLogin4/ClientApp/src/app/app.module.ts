import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'; // agm-direction
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { FetchCalendarComponent } from './fetch-calendar/fetch-calendar.component';
import { AddBookingComponent } from './add-booking/add-booking.component';
//import { MapComponent } from './map/map.component';
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    FetchCalendarComponent,
    AddBookingComponent,
    //MapComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({ //@agm/core
      apiKey: 'AIzaSyDdl92Jhloy3TczguBxXwqD5AH3m2oCNY0',
    }),
    AgmDirectionModule, //agm-direction
    ApiAuthorizationModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent, canActivate: [AuthorizeGuard] },
      { path: 'fetch-calendar', component: FetchCalendarComponent},
     // { path: 'map', component: MapComponent },
      { path: 'add-booking', component: AddBookingComponent },
    ])
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
